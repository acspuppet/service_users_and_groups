# The wallet group is used to back up wallet files to the KDCs.

class service_users_and_groups::group::wallet {
  group { 'wallet':
    ensure => present,
    gid    => 1055,
  }
}
