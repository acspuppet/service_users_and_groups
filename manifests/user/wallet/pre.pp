# Modeling for the 'wallet' user, for backing up wallet files onto the KDCs.

class service_users_and_groups::user::wallet::pre inherits service_users_and_groups::user::wallet {
  K5login['/srv/backups/wallet/.k5login'] {
    principals => [ 'service/wallet-pre@stanford.edu' ],
  }
}
