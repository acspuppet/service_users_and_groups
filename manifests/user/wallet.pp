# Modeling for the 'wallet' user, for backing up wallet files onto the KDCs.

class service_users_and_groups::user::wallet {
  include service_users_and_groups::group::wallet

  user { 'wallet':
    gid     => 'wallet',
    home    => '/srv/backups/wallet',
    uid     => 550,
    comment => 'Wallet backup user',
    shell   => '/usr/bin/rssh',
    require => Group['wallet'],
  }

  # Permissions and files for wallet home directory.
  file {
    '/srv/backups':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0775';
    '/srv/backups/wallet':
      ensure  => directory,
      owner   => 'wallet',
      group   => 'root',
      mode    => '0700',
      require => User['wallet'];
  }
  k5login { '/srv/backups/wallet/.k5login':
    principals => [ 'service/wallet@stanford.edu' ],
    purge      => true,
    require    => File['/srv/backups/wallet'],
  }
}

